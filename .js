function scanForThing() {
	return new Promise(function(resolve, reject){
		var emitter = scanLibrary.scan();
		
		emitter.on("discover", function(peripheral){
			resolve(peripheral);
		});
		
		emitter.on("error", function(err){
			reject(err);
		})
	})
}